<h1> Desafio Técnico Softplan </h1>

- O projeto a seguir foi criado utilizando o padrão de arquitetura de projetos de automação de testes <B>PAGE OBJECT</B>.

» Pré-requisitos para rodar esse projeto: 

1. Node.js: https://nodejs.org/en/download/
2. Visual Studio Code: https://code.visualstudio.com/download

» Passos para usar esse projeto:

1. Baixe ou clone esse repositório
2. Instale as dependências por meio do comando:<B> npm install</B>
3. Rode os testes com Cypress UI com o comando: <B> .\node_modules\.bin\cypress open </B> no terminal do VS Code

» Para geração de relatório

1. Para criar os relatórios é necessário ambiente Linux, rode o comando no terminal do VS Code: <B>npm run test</B>
